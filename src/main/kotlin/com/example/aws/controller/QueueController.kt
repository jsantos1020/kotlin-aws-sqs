package com.example.aws.controller

import com.example.aws.messaging.MessageSenderWithTemplate
import com.example.aws.model.Message
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("v1/producer")
class QueueController(private val messageSender: MessageSenderWithTemplate) {

    @PostMapping
    fun publicaEvento(@RequestBody message: Message) {
        messageSender.send(message.toString())
    }
}
