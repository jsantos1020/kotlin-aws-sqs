package com.example.aws.model

import java.io.Serializable

data class Message(val name: String, val content: String) : Serializable
