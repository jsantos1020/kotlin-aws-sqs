package com.example.aws

import com.example.aws.configuration.CreateQueue
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.ApplicationContext

@SpringBootApplication
class KotlinAwsSqsApplication

fun main(args: Array<String>) {
    val context: ApplicationContext = runApplication<KotlinAwsSqsApplication>(*args)
    createQueues(context = context)
}

private fun createQueues(context: ApplicationContext) {
    val createQueue = context.getBean(CreateQueue::class.java)
    createQueue.createStandardQueue("testQueue")
    createQueue.createStandardQueue("testQueue-Visibility")
}
