package com.example.aws.messaging

import io.awspring.cloud.messaging.core.QueueMessagingTemplate
import org.springframework.beans.factory.annotation.Value
import org.springframework.messaging.Message
import org.springframework.messaging.support.MessageBuilder
import org.springframework.stereotype.Component

@Component
class MessageSenderWithTemplate(
    private val queueMessagingTemplate: QueueMessagingTemplate,
    @Value("\${aws.queue.name}") private val queueName: String
) {

    fun send(messagePayload: String) {
        val msg: Message<String> = MessageBuilder.withPayload(messagePayload)
            .setHeader("TransactionId", "123")
            .setHeaderIfAbsent("country", "BR")
            .build()
        queueMessagingTemplate.convertAndSend<Any>(queueName, msg)
    }
}
