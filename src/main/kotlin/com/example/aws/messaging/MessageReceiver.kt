package com.example.aws.messaging

import io.awspring.cloud.messaging.listener.SqsMessageDeletionPolicy
import io.awspring.cloud.messaging.listener.Visibility
import io.awspring.cloud.messaging.listener.annotation.SqsListener
import org.slf4j.LoggerFactory
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Service

@Service
class MessageReceiver {

    private val logger = LoggerFactory.getLogger(javaClass)

    @SqsListener(value = ["\${aws.queue.name}"], deletionPolicy = SqsMessageDeletionPolicy.ON_SUCCESS)
    fun receiveMessage(@Payload message: String) {
        logger.info("Message: $message")
    }

    @SqsListener(value = ["testQueue-Visibility"], deletionPolicy = SqsMessageDeletionPolicy.NO_REDRIVE)
    fun receiveMessageVisibility(@Payload message: String, visibility: Visibility) {
        runCatching {
            logger.info("Message: $message")
        }.getOrElse { exception ->
            visibility.extend(60).get()
            throw exception
        }
    }
}
