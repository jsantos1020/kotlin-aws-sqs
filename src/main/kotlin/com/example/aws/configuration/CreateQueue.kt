package com.example.aws.configuration

import com.amazonaws.services.sqs.AmazonSQSAsync
import com.amazonaws.services.sqs.model.AmazonSQSException
import com.amazonaws.services.sqs.model.CreateQueueRequest
import org.springframework.stereotype.Component

@Component
class CreateQueue(private val amazonSQSAsync: AmazonSQSAsync) {

    fun createStandardQueue(standardQueueName: String?): String? {
        try {
            val standardQueueRequest = CreateQueueRequest()
                .withQueueName(standardQueueName)
                .addAttributesEntry("VisibilityTimeout", "60")
                .addAttributesEntry("DelaySeconds", "10")
                .addAttributesEntry("MessageRetentionPeriod", "86400")
            return amazonSQSAsync.createQueue(standardQueueRequest).queueUrl
        } catch (exception: AmazonSQSException) {
            if (exception.errorCode != "QueueAlreadyExists") {
                throw exception
            }
        }
        return null
    }
}
