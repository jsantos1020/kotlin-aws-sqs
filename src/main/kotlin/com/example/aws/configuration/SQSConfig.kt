package com.example.aws.configuration

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration
import com.amazonaws.services.sqs.AmazonSQSAsync
import com.amazonaws.services.sqs.AmazonSQSAsyncClientBuilder
import io.awspring.cloud.messaging.core.QueueMessagingTemplate
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class SQSConfig(
    @Value("\${aws.queue.endpoint}") private val endpoint: String,
    @Value("\${aws.region.static}") private val region: String
) {

    @Bean
    fun queueMessagingTemplate(
        amazonSQSAsync: AmazonSQSAsync
    ): QueueMessagingTemplate? {
        return QueueMessagingTemplate(amazonSQSAsync)
    }

    @Bean
    fun amazonSQS(): AmazonSQSAsync {
        return AmazonSQSAsyncClientBuilder.standard()
            .withCredentials(DefaultAWSCredentialsProviderChain())
            .withEndpointConfiguration(
                EndpointConfiguration(
                    endpoint,
                    region
                )
            )
            .build()
    }
}
